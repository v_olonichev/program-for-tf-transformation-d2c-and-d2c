/*
     Copyright (c) 2019 Vasiliy Olonichev v_olonichev@ksu.edu.ru
  
     This program is free software: you can redistribute it and/or
     modify it under the terms of the GNU General Public License as
     published by the Free Software Foundation, either version 2 of
     the License, or (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see
     <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include "./slycot.h"
#include "armadillo"
using namespace arma;

/*
N – order of the plants model;
dt – sampling period of discrete TF, dt1 – before and dt2 – after the transformation;
b – coefficients of the numerator, b1 – before and b2 – after the transformation;
a – coefficients of the denominator, a1 – before and a2 – after the transformation;
*/

// transfrom discrete TF to continuous
int d_to_c(int N, double dt, double *b1, double *a1, double *b2, double *a2){
    
    char rowcol='C';
    int m=1, p=1;
    int nr=0;
    int index=N;
    int lddcoe=1;
    int lduc01=1;
    int lduc02=1;
    double *dcoeff, *ucoeff;
    
    dcoeff=new double[index+1];
    dcoeff[0]=1.0;
    for(int i=1;i<=index;i++)
        dcoeff[i]=a1[i-1];
    
    ucoeff=new double[index+1];
    ucoeff[0]=0.0;
    for(int i=1;i<=index;i++)
        ucoeff[i]=b1[i-1];

    double *A;
    A=new double[index*index];
    int lda=index;
    double *B;
    B=new double[index];
    int ldb=index;
    double *C;
    int ldc=1;
    C=new double[index];
    double *D;
    int ldd=1;
    D=new double[1];
    double tol=-1.0;
    int *iwork;
    iwork=new int[index+1];
    double *dwork;
    int ldwork=index+index+4;
    dwork=new double[ldwork];
    int info;
	
	//transfrom from TF to SS
    td04ad_(&rowcol,&m,&p,&index,dcoeff,&lddcoe,ucoeff,&lduc01,&lduc02,&nr, A, &lda, B, &ldb, C, &ldc, D, &ldd, &tol, iwork, dwork, &ldwork, &info);
    
    mat Aa(index,index);
    mat Ba(index,1);
    
    for(int i=0;i<index;i++){
        for(int j=0;j<index;j++){
            Aa(i,j)=A[index*j+i];
        }
    }
    for(int i=0;i<index;i++){
        Ba(i,0)=B[i];
    }
    
    //make expanded matrix
    mat tmp1=join_horiz(Aa,Ba);
    mat tmp2=join_horiz(mat(1,index).zeros(),mat(1,1).eye());
    mat tmp=join_vert(tmp1,tmp2);
    cx_mat tmpc=cx_mat(tmp,mat(index+1,index+1).zeros());
    cx_mat Sx=logmat(tmpc);
	
	//transfrom expanded SS matrix from discrete form into continuous
    mat S=real(Sx);
    S=S/dt;
	
	//extract matrix A and vector B
    for(int i=0;i<index;i++){
        for(int j=0;j<index;j++){
            A[index*j+i]=S(i,j);
        }
    }
    for(int i=0;i<index;i++){
        B[i]=S(i,index);
    }
    int n=index;
    double *diwork;
    diwork=new double[index+1];
    delete[] dwork;
    ldwork=n*(n+1)+n*m+2*n+n;
    dwork=new double[ldwork];
    
	//transform from SS to TF
    tb04ad_(&rowcol,&n,&m,&p,A,&n,B,&n,C,&p,D,&p,&nr,&index,dcoeff,&lddcoe,ucoeff,&lduc01,&lduc02,&tol,&tol,diwork,dwork,&ldwork,&info);
	
	//get results
    for(int i=0;i<index;i++){
        a2[i]=dcoeff[i+1];
        b2[i]=ucoeff[i+1];
    }
    delete[] dwork;
    delete[] diwork;
    delete[] iwork;
    delete[] ucoeff;
    delete[] dcoeff;
    delete[] A;
    delete[] B;
    delete[] C;
    delete[] D;
    return 0;
}

// transfrom discrete TF to discrete with another sampling period
int d_to_d(int N, double dt1, double dt2, double *b1, double *a1, double *b2, double *a2){
    
    char rowcol='C';
    int m=1, p=1;
    int nr=0;
    int index=N;
    int lddcoe=1;
    int lduc01=1;
    int lduc02=1;
    double *dcoeff, *ucoeff;
    
    dcoeff=new double[index+1];
    dcoeff[0]=1.0;
    for(int i=1;i<=index;i++)
        dcoeff[i]=a1[i-1];
    
    ucoeff=new double[index+1];
    ucoeff[0]=0.0;
    for(int i=1;i<=index;i++)
        ucoeff[i]=b1[i-1];

    double *A;
    A=new double[index*index];
    int lda=index;
    double *B;
    B=new double[index];
    int ldb=index;
    double *C;
    int ldc=1;
    C=new double[index];
    double *D;
    int ldd=1;
    D=new double[1];
    double tol=-1.0;
    int *iwork;
    iwork=new int[index+1];
    double *dwork;
    int ldwork=index+index+4;
    dwork=new double[ldwork];
    int info;
	
	//transfrom from TF to SS
    td04ad_(&rowcol,&m,&p,&index,dcoeff,&lddcoe,ucoeff,&lduc01,&lduc02,&nr, A, &lda, B, &ldb, C, &ldc, D, &ldd, &tol, iwork, dwork, &ldwork, &info);
    
    mat Aa(index,index);
    mat Ba(index,1);
    
    for(int i=0;i<index;i++){
        for(int j=0;j<index;j++){
            Aa(i,j)=A[index*j+i];
        }
    }
    for(int i=0;i<index;i++){
        Ba(i,0)=B[i];
    }
    
    //make expanded matrix
    mat tmp1=join_horiz(Aa,Ba);
    mat tmp2=join_horiz(mat(1,index).zeros(),mat(1,1).eye());
    mat tmp=join_vert(tmp1,tmp2);
    cx_mat tmpc=cx_mat(tmp,mat(index+1,index+1).zeros());
	
	//transfrom SS expanded matrix to another sampling period
    cx_mat Sx=logmat(tmpc);
    mat S1=real(Sx);
    mat S2=S1/dt1*dt2;
	mat S=expmat(S2);
	
	//extract matrix A and vector B
    for(int i=0;i<index;i++){
        for(int j=0;j<index;j++){
            A[index*j+i]=S(i,j);
        }
    }
    for(int i=0;i<index;i++){
        B[i]=S(i,index);
    }
    
    int n=index;
    double *diwork;
    diwork=new double[index+1];
    delete[] dwork;
    ldwork=n*(n+1)+n*m+2*n+n;
    dwork=new double[ldwork];
	
    //transform from SS to TF
    tb04ad_(&rowcol,&n,&m,&p,A,&n,B,&n,C,&p,D,&p,&nr,&index,dcoeff,&lddcoe,ucoeff,&lduc01,&lduc02,&tol,&tol,diwork,dwork,&ldwork,&info);
	
	//get results
    for(int i=0;i<index;i++){
        a2[i]=dcoeff[i+1];
        b2[i]=ucoeff[i+1];
    }
    delete[] dwork;
    delete[] diwork;
    delete[] iwork;
    delete[] ucoeff;
    delete[] dcoeff;
    delete[] A;
    delete[] B;
    delete[] C;
    delete[] D;
    return 0;
}

// transfrom continuous TF to discrete
int c_to_d(int N, double *b1, double *a1, double dt, double *b2, double *a2){
    char rowcol='C';
    int m=1, p=1;
    int nr=0;
    int index=N;
    int lddcoe=1;
    int lduc01=1;
    int lduc02=1;
    double *dcoeff, *ucoeff;
    
    dcoeff=new double[index+1];
    dcoeff[0]=1.0;
    for(int i=1;i<=N;i++)
        dcoeff[i]=a1[i-1];
    
    ucoeff=new double[index+1];
    ucoeff[0]=0.0;
    for(int i=1;i<=N;i++)
        ucoeff[i]=b1[i-1];
    double *A;
    A=new double[index*index];
    int lda=index;
    double *B;
    B=new double[index];
    int ldb=index;
    double *C;
    int ldc=1;
    C=new double[index];
    double *D;
    int ldd=1;
    D=new double[1];
    double tol=-1.0;
    int *iwork;
    iwork=new int[index+1];
    double *dwork;
    int ldwork=index+index+4;
    dwork=new double[ldwork];
    int info;
	
    //transfrom from TF to SS
    td04ad_(&rowcol,&m,&p,&index,dcoeff,&lddcoe,ucoeff,&lduc01,&lduc02,
                    &nr, A, &lda, B, &ldb, C, &ldc, D, &ldd,
                    &tol, iwork, dwork, &ldwork, &info);

    mat Aa(index,index);
    mat Ba(index,1);
    for(int i=0;i<index;i++){
        for(int j=0;j<index;j++){
            Aa(i,j)=A[index*j+i];
        }
    }
    for(int i=0;i<index;i++){
        Ba(i,0)=B[i];
    }

    //make expanded matrix
    mat tmp=join_horiz(Aa,Ba);
    tmp=join_vert(tmp,mat(1,index+1).zeros());

	//transfrom expanded SS matrix from continuous form into discrete
    tmp=tmp*dt;
    mat S=expmat(tmp);
	
	//extract matrix A and vector B
    for(int i=0;i<index;i++){
        for(int j=0;j<index;j++){
            A[index*j+i]=S(i,j);
        }
    }
    for(int i=0;i<index;i++){
        B[i]=S(i,index);
    }
    int n=index;
    double *diwork;
    diwork=new double[index+1];
    delete[] dwork;
    ldwork=n*(n+1)+n*m+2*n+n;
    dwork=new double[ldwork];
    
    //transform from SS to TF
    tb04ad_(&rowcol,&n,&m,&p,A,&n,B,&n,C,&p,D,&p,&nr,&index,
            dcoeff,&lddcoe,ucoeff,&lduc01,&lduc02,&tol,&tol,
            diwork,dwork,&ldwork,&info);
    
	//get results
	for(int i=0;i<index;i++){
        a2[i]=dcoeff[i+1];
        b2[i]=ucoeff[i+1];
    }
    delete[] dwork;
    delete[] diwork;
    delete[] iwork;
    delete[] ucoeff;
    delete[] dcoeff;
    delete[] A;
    delete[] B;
    delete[] C;
    delete[] D;
    return 0;
}
