#ifndef _SLYCOT_
#define _SLYCOT_

/*
SUBROUTINE TD04AD( ROWCOL, M, P, INDEX, DCOEFF, LDDCOE, UCOEFF,
     $                   LDUCO1, LDUCO2, NR, A, LDA, B, LDB, C, LDC, D,
     $                   LDD, TOL, IWORK, DWORK, LDWORK, INFO ) 
    Convert a tranfer function or matrix of transfer functions to
    a minimum state space realization.
*/     
extern "C"
void* td04ad_(
              char *rowcol, 
              int *m,       
              int *p,       
              int *index,   
              double* dcoeff,
              int *lddcoe,  
              double* ucoeff,
              int *lduc01,  
              int *lduc02,  
              int *nr,      
              double*A,
              int *lda,     
              double*B,
              int *ldb,     
              double*C,
              int *ldc,     
              double*D,
              int *ldd,     
              double *tol,  
              int *iwork,   
              double* dwork,
              int *ldwork,  
              int *info     
            );

/*
      SUBROUTINE TB04AD( ROWCOL, N, M, P, A, LDA, B, LDB, C, LDC, D,
     $                   LDD, NR, INDEX, DCOEFF, LDDCOE, UCOEFF, LDUCO1,
     $                   LDUCO2, TOL1, TOL2, IWORK, DWORK, LDWORK,
     $                   INFO )
C     To find the transfer matrix T(s) of a given state-space
C     representation (A,B,C,D). T(s) is expressed as either row or
C     column polynomial vectors over monic least common denominator
C     polynomials.
*/

extern "C"
void tb04ad_(char *rowcol,  
             int *n,        
             int *m,        
             int *p,        
             double* A,     
             int *lda,      
             double *B,     
             int *ldb,      
             double *C,     
             int *ldc,      
             double *D,     
             int *ldd,      
             int *nr,       
             int *index,    
             double *dcoeff,
             int *lddcoe,   
             double *ucoef, 
             int *lduc01,   
             int *lduc02,   
             double *tol1,  
             double *tol2,  
             double *iwork, 
             double *dwork, 
             int *ldwork,   
             int *info      
     );
#endif
