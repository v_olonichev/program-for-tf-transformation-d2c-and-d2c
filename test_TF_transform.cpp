#include <stdio.h>
#include <math.h>
#include "c2d_n_d2c.h"

void print_tf(int N, double *num, double *den, char p){
	int n=0;
	char s;
	char nums[80];
	for(int i=0;i<N-1;i++){
		s=num[i+1]>0? '+':'-';
		n+=sprintf(nums+n,"%g %c^%d %c ",fabs(num[i]),p,N-1-i,s);
	}
	n+=sprintf(nums+n,"%g",fabs(num[N-1]));
	int d=0;
	char dens[80];
	s=den[0]>0? '+':'-';
	d+=sprintf(dens,"%c^%d %c ",p,N,s);
	for(int i=0;i<N-1;i++){
		s=den[i+1]>0? '+':'-';
		d+=sprintf(dens+d,"%g %c^%d %c ",fabs(den[i]),p,N-1-i,s);
	}
	d+=sprintf(dens+d,"%g",fabs(den[N-1]));
	
	int l=d>n ? d:n;
	if(l>n) for(int i=0;i<(l-n)/2;i++)printf(" ");
	printf("%s\n",nums);
	for(int i=0;i<l;i++)printf("-");
	if(l>d) for(int i=0;i<(l-d)/2;i++)printf(" ");
	printf("\n%s\n",dens);
	return;
}

int main(){
	int N=4;
	double dt=1.4;
	double bd1[]={0.901018,-0.840538,0.766549,-0.148877};
	double ad1[]={-3.152926, 3.765654, -2.01602, 0.406843};
	double *bc, *ac, *bd2, *ad2;
	printf("Testing transfer functions coversion\n\n");
	printf("Original discrete transfer function with T=%g\n",dt);
	print_tf(N,bd1,ad1,'z');
	printf("\n");
    bc=new double[N];
    ac=new double[N];
    bd2=new double[N];
    ad2=new double[N];
    int ra=d_to_c(N,dt,bd1,ad1,bc,ac);
	printf("Continuous transfer function from discrete one\n");
	print_tf(N,bc,ac,'s');
    printf("\n");
    int rd=c_to_d(N,bc,ac,dt,bd2,ad2);
	printf("Restored discrete transfer function from continuous one\n");
	print_tf(N,bd2,ad2,'z');
    delete[] bc;
    delete[] ac;
    delete[] bd2;
    delete[] ad2;
	printf("\n");
	N=3;
	double dt1=1.0;
	double dt2=3.0;
	double bd[]={0.2505,-0.113, 0.06322};
	double ad[]={-1.669, 0.8019, -0.03087};
	printf("Original discrete transfer function with T=%g\n",dt1);
	print_tf(N,bd,ad,'z');
    printf("\n");
    bd2=new double[N];
    ad2=new double[N];
    bc=new double[N];
    ac=new double[N];
    int rd1=d_to_d(N,dt1,dt2,bd,ad,bc,ac);
	printf("Discrete transfer function with new T=%g from discrete one\n",dt2);
	print_tf(N,bc,ac,'z');
    printf("\n");
    int rd2=d_to_d(N,dt2,dt1,bc,ac,bd2,ad2);
	printf("Restored discrete transfer function\n");
	print_tf(N,bd2,ad2,'z');
    delete[] bc;
    delete[] ac;
    delete[] bd2;
    delete[] ad2;
    printf("\n");
    return 0;
}
