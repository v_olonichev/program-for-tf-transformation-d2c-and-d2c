CC = g++

all: test_TF_transform use_TF_transform c2d_n_d2c/c2d_n_d2c.o

ifeq ($(shell uname -i), x86_64)
    OBJ = c2d_n_d2c/c2d_n_d2c.o AMD64/slycot.a
endif
ifeq ($(shell uname -i), armv7l)
    OBJ = c2d_n_d2c/c2d_n_d2c.o ARMv7/slycot.a
endif

LDLIB = -llapack -lblas

test_TF_transform: test_TF_transform.o c2d_n_d2c/c2d_n_d2c.o
	$(CC) -o test_TF_transform test_TF_transform.o $(OBJ) $(LDLIB)

test_TF_transform.o: test_TF_transform.cpp
	$(CC) -c test_TF_transform.cpp

use_TF_transform: use_TF_transform.o c2d_n_d2c/c2d_n_d2c.o
	$(CC) -o use_TF_transform use_TF_transform.o $(OBJ) $(LDLIB)

use_TF_transform.o: use_TF_transform.cpp
	$(CC) -c use_TF_transform.cpp

c2d_n_d2c/c2d_n_d2c.o:
	$(MAKE) -C c2d_n_d2c
