#include <stdio.h>
#include <math.h>
#include "c2d_n_d2c.h"

int N=4;
double dt1;
double dt2;
double *b1;
double *a1;
double *b2;
double *a2;

void print_tf(int N, double *num, double *den, char p){
	int n=0;
	char s;
	char nums[80];
	for(int i=0;i<N-1;i++){
		s=num[i+1]>0? '+':'-';
		n+=sprintf(nums+n,"%g %c^%d %c ",fabs(num[i]),p,N-1-i,s);
	}
	n+=sprintf(nums+n,"%g",fabs(num[N-1]));
	int d=0;
	char dens[80];
	s=den[0]>0? '+':'-';
	d+=sprintf(dens,"%c^%d %c ",p,N,s);
	for(int i=0;i<N-1;i++){
		s=den[i+1]>0? '+':'-';
		d+=sprintf(dens+d,"%g %c^%d %c ",fabs(den[i]),p,N-1-i,s);
	}
	d+=sprintf(dens+d,"%g",fabs(den[N-1]));
	
	int l=d>n ? d:n;
	if(l>n) for(int i=0;i<(l-n)/2;i++)printf(" ");
	printf("%s\n",nums);
	for(int i=0;i<l;i++)printf("-");
	if(l>d) for(int i=0;i<(l-d)/2;i++)printf(" ");
	printf("\n%s\n",dens);
	return;
}

int main(int argc, char* argv[]){
	if(argc==1){
		printf("file name with TF expected\n");
		printf("  N\n num\n den\n dt1\n dt2\n");
		return 1;
	}
	FILE *f=fopen(argv[1],"r");
	if(!f){
		printf("file not found\n");
		return 2;
	}
	int r1=fscanf(f,"%d",&N);
    b1=new double[N];
    a1=new double[N];
    b2=new double[N];
    a2=new double[N];
	for(int i=0;i<N;i++)
		fscanf(f,"%lf",&b1[i]);
	for(int i=0;i<N;i++)
		fscanf(f,"%lf",&a1[i]);
	int r2=fscanf(f,"%lf",&dt1);
	int r3=fscanf(f,"%lf",&dt2);
	if((r1==0)||(r2==0)||(r3==0)){
		printf("bad file\n");
		return 3;
	}
	if(dt1==dt2){
		printf("dt1=dt2=%g\n",dt1);
		return 4;
	}
	printf("Transfer functions coversion\n\n");
	char c1=(dt1==0.0) ? 's':'z';
	char c2=(dt2==0.0) ? 's':'z';
	printf("Original transfer function with T=%g\n",dt1);
	print_tf(N,b1,a1,c1);
	printf("\n");
	if(dt2==0.0){
		r1=d_to_c(N,dt1,b1,a1,b2,a2);
	}
	else{
		if(dt1==0.0){
			r2=c_to_d(N,b1,a1,dt2,b2,a2);
		}
		else{
			r3=d_to_d(N,dt1,dt2,b1,a1,b2,a2);
		}
	}
	printf("Result transfer function with T=%g\n",dt2);
	print_tf(N,b2,a2,c2);
    delete[] b1;
    delete[] a1;
    delete[] b2;
    delete[] a2;
    return 0;
}
