#some systems require -fpic flag  for position independent code
gcc -O2 -fpic -c *.f
ar rcs slycot.a *.o
rm *.o
#if you do not want to see source code
#rm *.f
