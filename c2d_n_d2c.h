#ifndef _C2D_D2C_
#define _C2D_D2C_
/*
N – order of the plants model;
dt – sampling period of discrete TF, dt1 – before and dt2 – after the transformation;
b – coefficients of the numerator, b1 – before and b2 – after the transformation;
a – coefficients of the denominator, a1 – before and a2 – after the transformation;
*/
int d_to_c(int N, double dt, double *b1, double *a1, double *b2, double *a2);
int c_to_d(int N, double *b1, double *a1, double dt, double *b2, double *a2);
int d_to_d(int N, double dt1, double dt2, double *b1, double *a1, double *b2, double *a2);
#endif
